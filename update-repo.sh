#!/bin/bash
set -euo pipefail

echo "Pulling the latest changes from the remote repository..."
git pull

echo "Staging all changes..."
git add --all

# Prompt the user for a commit message
read -rp "Enter your commit message: " commit_message

# Ensure that a commit message was provided
if [[ -z "$commit_message" ]]; then
    echo "Error: Commit message cannot be empty. Aborting commit."
    exit 1
fi

echo "Committing changes..."
git commit -m "$commit_message"

echo "Pushing changes to the remote repository..."
git push

echo "Git operations completed successfully."
