#!/usr/bin/env python3

import os
import sys
import logging
import subprocess
import getpass
import time

def check_root():
    if os.geteuid() != 0:
        sys.exit("Must run as root.")

def check_uefi():
    if not os.path.exists("/sys/firmware/efi"):
        sys.exit("Not in UEFI mode: /sys/firmware/efi missing.")

def udev_settle():
    logging.info("Running: udevadm settle")
    subprocess.run(["udevadm", "settle"], check=True)

def ensure_required_packages():
    required_packages = ["parted", "efibootmgr"]
    missing = []
    for pkg in required_packages:
        result = subprocess.run(
            ["pacman", "-Qi", pkg],
            stdout=subprocess.DEVNULL,
            stderr=subprocess.DEVNULL
        )
        if result.returncode != 0:
            missing.append(pkg)

    if missing:
        logging.info("Missing live packages: %s", " ".join(missing))
        subprocess.run(["pacman", "-Sy", "--noconfirm"] + missing, check=True)
    else:
        logging.info("All required packages are already installed in the live environment.")

def modify_pacman_conf():
    pacman_conf_path = "/etc/pacman.conf"
    logging.info("Modifying %s to set ParallelDownloads=20", pacman_conf_path)
    try:
        with open(pacman_conf_path, "r") as f:
            lines = f.readlines()
    except Exception as e:
        sys.exit(f"Failed to read {pacman_conf_path}: {e}")

    new_lines = []
    found = False
    for line in lines:
        if line.strip().startswith("ParallelDownloads"):
            new_lines.append("ParallelDownloads = 20\n")
            found = True
        else:
            new_lines.append(line)

    if not found:
        inserted = False
        for i, line in enumerate(new_lines):
            if line.strip() == "[options]":
                new_lines.insert(i + 1, "ParallelDownloads = 15\n")
                inserted = True
                break
        if not inserted:
            new_lines.insert(0, "ParallelDownloads = 15\n")

    try:
        with open(pacman_conf_path, "w") as f:
            f.writelines(new_lines)
    except Exception as e:
        sys.exit(f"Failed to write to %s: %s" % (pacman_conf_path, e))

class ArchInstaller:
    def __init__(self, args):
        self.efi_size     = args.efi_size
        self.hostname     = args.hostname
        self.locale       = args.locale
        self.timezone     = args.timezone
        self.disk         = args.disk
        self.username     = args.username
        self.userpassword = args.userpassword
        self.rootpassword = args.rootpassword
        self.kernel       = args.kernel
        self.minimal      = args.minimal
        self.rootspec     = args.rootspec

    # ------------------ Shared Utility --------------------

    def run_in_chroot(self, cmd):
        logging.info("Running in chroot: %s", cmd)
        subprocess.run(["arch-chroot", "/mnt", "/bin/bash", "-c", cmd], check=True)

    # ------------------ Credential Steps ------------------

    def prompt_credentials(self):
        if not self.username:
            self.username = input("Username: ")
        if not self.userpassword:
            while True:
                pw1 = getpass.getpass(f"Password for {self.username}: ")
                pw2 = getpass.getpass("Confirm: ")
                if pw1 == pw2:
                    self.userpassword = pw1
                    break
                else:
                    print("Mismatch, try again.")
        if not self.rootpassword:
            while True:
                pw1 = getpass.getpass("Root password: ")
                pw2 = getpass.getpass("Confirm: ")
                if pw1 == pw2:
                    self.rootpassword = pw1
                    break
                else:
                    print("Mismatch, try again.")

    def prompt_root_password_minimal(self):
        if not self.rootpassword:
            while True:
                pw1 = getpass.getpass("Root password (minimal): ")
                pw2 = getpass.getpass("Confirm: ")
                if pw1 == pw2:
                    self.rootpassword = pw1
                    break
                else:
                    print("Mismatch, try again.")

    # ------------------ Disk Preparation ------------------

    def detect_disk(self):
        if self.disk:
            if not os.path.exists(self.disk):
                sys.exit(f"Disk {self.disk} does not exist.")
            return
        for candidate in ["/dev/vda", "/dev/nvme0n1", "/dev/sda"]:
            if os.path.exists(candidate):
                self.disk = candidate
                break
        if not self.disk:
            sys.exit("No disk found.")
        logging.info("Using disk: %s", self.disk)

    def partition_disk(self):
        logging.info(f"Partitioning {self.disk} with GPT (EFI + root)...")
        subprocess.run(["parted", "--script", self.disk, "mklabel", "gpt"], check=True)
        subprocess.run(["parted", "--script", self.disk, "mkpart", "primary", "fat32", "1MiB", self.efi_size], check=True)
        subprocess.run(["parted", "--script", self.disk, "set", "1", "esp", "on"], check=True)
        subprocess.run(["parted", "--script", self.disk, "mkpart", "primary", "ext4", self.efi_size, "100%"], check=True)
        udev_settle()
        time.sleep(3)
        logging.info("Disk partitioning complete.")

    def define_partitions(self):
        if "nvme" in self.disk:
            self.efi_part  = self.disk + "p1"
            self.root_part = self.disk + "p2"
        else:
            self.efi_part  = self.disk + "1"
            self.root_part = self.disk + "2"
        logging.info(f"Defined partitions: EFI={self.efi_part}, Root={self.root_part}")

    def format_partitions(self):
        logging.info(f"Formatting {self.efi_part} as FAT32...")
        subprocess.run(["mkfs.fat", "-F32", self.efi_part], check=True)
        logging.info(f"Formatting {self.root_part} as ext4...")
        subprocess.run(["mkfs.ext4", "-F", self.root_part], check=True)
        logging.info("Partitions formatted.")

    def mount_partitions(self):
        logging.info("Mounting partitions...")
        os.makedirs("/mnt", exist_ok=True)
        subprocess.run(["mount", self.root_part, "/mnt"], check=True)
        os.makedirs("/mnt/boot", exist_ok=True)
        subprocess.run(["mount", self.efi_part, "/mnt/boot"], check=True)
        logging.info("Partitions mounted.")

    # ------------------ Base Install ----------------------

    def install_base_system(self):
        logging.info("Enabling NTP with timedatectl set-ntp true")
        subprocess.run(["timedatectl", "set-ntp", "true"], check=True)

        if self.kernel == "linux":
            kpkgs = ["linux", "linux-headers"]
        else:
            kpkgs = ["linux-lts", "linux-lts-headers"]

        base = [
            "base", "base-devel",
            *kpkgs,
            "linux-firmware",
            "intel-ucode",
            "sudo",
            "networkmanager"
        ]

        logging.info("Installing base packages: %s", " ".join(base))
        subprocess.run(["pacstrap", "/mnt"] + base, check=True)
        logging.info("Base system installed.")

    def generate_fstab(self):
        logging.info("Generating fstab...")
        cmd = "genfstab -U /mnt >> /mnt/etc/fstab"
        subprocess.run(cmd, shell=True, check=True)

        fstab_path = "/mnt/etc/fstab"
        if not os.path.exists(fstab_path) or os.path.getsize(fstab_path) == 0:
            sys.exit("fstab generation failed.")
        logging.info("fstab generated.")

    def generate_initramfs(self):
        logging.info("Generating initramfs with mkinitcpio -P...")
        self.run_in_chroot("mkinitcpio -P")
        logging.info("Initramfs generated.")

    # ------------------ Configuration ---------------------

    def configure_system(self):
        logging.info("Configuring system settings (timezone, locale, hostname, etc.)")
        etc_dir = "/mnt/etc"
        zoneinfo_path = f"/usr/share/zoneinfo/{self.timezone}"
        localtime_path = os.path.join("/mnt", "etc", "localtime")

        if os.path.exists(zoneinfo_path):
            if os.path.exists(localtime_path):
                os.remove(localtime_path)
            os.symlink(zoneinfo_path, localtime_path)
        else:
            logging.warning("Timezone file not found: %s", zoneinfo_path)

        # /etc/locale.gen
        with open(os.path.join(etc_dir, "locale.gen"), "w") as f:
            f.write(f"{self.locale} UTF-8\n")

        # /etc/locale.conf
        with open(os.path.join(etc_dir, "locale.conf"), "w") as f:
            f.write(f"LANG={self.locale}\n")

        # /etc/hostname
        with open(os.path.join(etc_dir, "hostname"), "w") as f:
            f.write(self.hostname + "\n")

        # /etc/hosts
        hosts_path = os.path.join(etc_dir, "hosts")
        with open(hosts_path, "w") as f:
            f.write("127.0.0.1\tlocalhost\n")
            f.write("::1\tlocalhost\n")
            f.write(f"127.0.1.1\t{self.hostname}.localdomain {self.hostname}\n")

        # Run locale-gen & hwclock in the chroot
        self.run_in_chroot("locale-gen")
        self.run_in_chroot("hwclock --systohc")
        logging.info("System configuration complete.")

    def configure_network(self):
        logging.info("Enabling NetworkManager in chroot...")
        cmd = "pacman -Sy --noconfirm networkmanager && systemctl enable NetworkManager"
        self.run_in_chroot(cmd)
        logging.info("NetworkManager enabled.")

    # ------------------ Bootloader ------------------------

    def install_systemd_boot(self):
        logging.info("Installing systemd-boot...")
        efi_mount = "/mnt/boot"
        if not os.path.exists(efi_mount):
            sys.exit(f"EFI mount point {efi_mount} not found.")
        if not os.path.ismount("/sys/firmware/efi/efivars"):
            logging.info("Mounting efivarfs")
            subprocess.run(["mount", "-t", "efivarfs", "efivarfs", "/sys/firmware/efi/efivars"], check=True)

        subprocess.run(["bootctl", "--path=" + efi_mount, "install"], check=True)
        udev_settle()
        time.sleep(3)
        logging.info("systemd-boot installed.")

        root_spec = self._detect_root_spec()
        if not root_spec:
            if self.rootspec:
                root_spec = self.rootspec
            else:
                sys.exit("Could not detect root=; supply --rootspec.")

        loader_dir = os.path.join(efi_mount, "loader")
        os.makedirs(loader_dir, exist_ok=True)
        with open(os.path.join(loader_dir, "loader.conf"), "w") as f:
            f.write("default arch\n")
            f.write("timeout 5\n")
            f.write("editor 0\n")

        entry_dir = os.path.join(loader_dir, "entries")
        os.makedirs(entry_dir, exist_ok=True)
        entry_conf_path = os.path.join(entry_dir, "arch.conf")
        entry_contents = f"""title   Arch Linux
linux   /vmlinuz-{self.kernel}
initrd  /intel-ucode.img
initrd  /initramfs-{self.kernel}.img
options root={root_spec} rw
"""
        with open(entry_conf_path, "w") as f:
            f.write(entry_contents)
        logging.info("Bootloader configuration written.")

    def _detect_root_spec(self):
        root_spec = ""
        try:
            partuuid = subprocess.check_output(
                ["blkid", "-s", "PARTUUID", "-o", "value", self.root_part]
            ).decode().strip()
            if partuuid:
                root_spec = f"PARTUUID={partuuid}"
        except Exception as e:
            logging.debug("Failed to get PARTUUID: %s", e)

        if not root_spec:
            try:
                fsuuid = subprocess.check_output(
                    ["blkid", "-s", "UUID", "-o", "value", self.root_part]
                ).decode().strip()
                if fsuuid:
                    root_spec = f"UUID={fsuuid}"
            except Exception as e:
                logging.debug("Failed to get UUID: %s", e)

        return root_spec

    # ------------------ User Setup ------------------------

    def set_root_password(self):
        logging.info("Setting root password...")
        cmd = f'echo "root:{self.rootpassword}" | chpasswd'
        self.run_in_chroot(cmd)

    def create_user(self):
        logging.info(f"Creating user {self.username}...")
        cmd = (
            f"useradd -m -G audio,video,network,wheel,storage,rfkill -s /bin/bash {self.username} && "
            f'echo "{self.username}:{self.userpassword}" | chpasswd'
        )
        self.run_in_chroot(cmd)

    def configure_sudo(self):
        logging.info("Configuring sudo privileges for wheel group...")
        cmd = "sed -i '/^# %wheel ALL=(ALL:ALL) ALL/s/^# //' /etc/sudoers"
        self.run_in_chroot(cmd)

    def modify_pacman_conf_in_target(self):
        pacman_conf_path = "/mnt/etc/pacman.conf"
        logging.info("Modifying %s in the target system to set ParallelDownloads=20", pacman_conf_path)
        try:
            with open(pacman_conf_path, "r") as f:
                lines = f.readlines()
        except Exception as e:
            sys.exit(f"Failed to read {pacman_conf_path}: {e}")

        new_lines = []
        found = False
        for line in lines:
            stripped = line.strip()
            if stripped.startswith("ParallelDownloads"):
                new_lines.append("ParallelDownloads = 20\n")
                found = True
            else:
                new_lines.append(line)

        if not found:
            inserted = False
            for i, line in enumerate(new_lines):
                # Insert after [options] if found
                if line.strip() == "[options]":
                    new_lines.insert(i + 1, "ParallelDownloads = 20\n")
                    inserted = True
                    break
            if not inserted:
                new_lines.insert(0, "ParallelDownloads = 20\n")

        try:
            with open(pacman_conf_path, "w") as f:
                f.writelines(new_lines)
        except Exception as e:
            sys.exit(f"Failed to write to {pacman_conf_path}: {e}")

        logging.info("Set ParallelDownloads=15 in /mnt/etc/pacman.conf successfully.")

    # ------------------ Post-Install Steps ----------------

    def add_additional_repos(self):
        logging.info("Adding ArcoLinux and Chaotic-AUR repos in chroot.")

        self.run_in_chroot("pacman -S wget --noconfirm --needed")

        self.run_in_chroot(
            "wget https://github.com/arcolinux/arcolinux_repo/raw/main/x86_64/arcolinux-keyring-20251209-3-any.pkg.tar.zst "
            "-O /root/arcolinux-keyring.pkg.tar.zst"
        )
        self.run_in_chroot("pacman -U --noconfirm --needed /root/arcolinux-keyring.pkg.tar.zst")

        self.run_in_chroot(
            "wget https://github.com/arcolinux/arcolinux_repo/raw/main/x86_64/arcolinux-mirrorlist-git-24.03-12-any.pkg.tar.zst "
            "-O /root/arcolinux-mirrorlist.pkg.tar.zst"
        )
        self.run_in_chroot("pacman -U --noconfirm --needed /root/arcolinux-mirrorlist.pkg.tar.zst")

        self.run_in_chroot(
            "wget https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-keyring.pkg.tar.zst "
            "-O /root/chaotic-keyring.pkg.tar.zst"
        )
        self.run_in_chroot("pacman -U --noconfirm --needed /root/chaotic-keyring.pkg.tar.zst")

        self.run_in_chroot(
            "wget https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-mirrorlist.pkg.tar.zst "
            "-O /root/chaotic-mirrorlist.pkg.tar.zst"
        )
        self.run_in_chroot("pacman -U --noconfirm --needed /root/chaotic-mirrorlist.pkg.tar.zst")

        self.run_in_chroot("echo >> /etc/pacman.conf")
        self.run_in_chroot("echo '[arcolinux_repo]' >> /etc/pacman.conf")
        self.run_in_chroot("echo 'SigLevel = PackageRequired DatabaseNever' >> /etc/pacman.conf")
        self.run_in_chroot("echo 'Include = /etc/pacman.d/arcolinux-mirrorlist' >> /etc/pacman.conf")

        self.run_in_chroot("echo >> /etc/pacman.conf")
        self.run_in_chroot("echo '[arcolinux_repo_3party]' >> /etc/pacman.conf")
        self.run_in_chroot("echo 'SigLevel = PackageRequired DatabaseNever' >> /etc/pacman.conf")
        self.run_in_chroot("echo 'Include = /etc/pacman.d/arcolinux-mirrorlist' >> /etc/pacman.conf")

        self.run_in_chroot("echo >> /etc/pacman.conf")
        self.run_in_chroot("echo '[arcolinux_repo_xlarge]' >> /etc/pacman.conf")
        self.run_in_chroot("echo 'SigLevel = PackageRequired DatabaseNever' >> /etc/pacman.conf")
        self.run_in_chroot("echo 'Include = /etc/pacman.d/arcolinux-mirrorlist' >> /etc/pacman.conf")

        self.run_in_chroot("echo >> /etc/pacman.conf")
        self.run_in_chroot("echo '[chaotic-aur]' >> /etc/pacman.conf")
        self.run_in_chroot("echo 'SigLevel = Required DatabaseOptional' >> /etc/pacman.conf")
        self.run_in_chroot("echo 'Include = /etc/pacman.d/chaotic-mirrorlist' >> /etc/pacman.conf")

        self.run_in_chroot("echo >> /etc/pacman.conf")
        self.run_in_chroot("echo '[natto-repo]' >> /etc/pacman.conf")
        self.run_in_chroot("echo 'SigLevel = Optional TrustAll' >> /etc/pacman.conf")
        self.run_in_chroot("echo 'Server = https://gitlab.com/samurailostinjapan/$repo/-/raw/main/$arch' >> /etc/pacman.conf")

        self.run_in_chroot("pacman -Sy --noconfirm")
        logging.info("ArcoLinux & Chaotic-AUR & Natto-repo repositories have been added successfully.")

    def install_display_manager(self):
        logging.info("Installing display manager + Xorg...")
        self.run_in_chroot("pacman -S --noconfirm --needed sddm xorg-server xorg-apps xorg-xinit")
        self.run_in_chroot("systemctl enable sddm.service")
        logging.info("Display manager installed and enabled.")

    def install_bspwm(self):
        packages = [
            "arcolinux-root-git", "arconet-wallpapers", "feh",  "man-db", "man-pages", "mkinitcpio-firmware", "mlocate", "most", "polkit-gnome",
            "alacritty", "alacritty-themes", "arandr", "bash-completion", "bat", "bibata-cursor-theme-bin", "bspwm", "dunst", "downgrade", "feh",
            "archlinux-logout-git", "archlinux-tweak-tool-git", "arcolinux-bin-git", "arcolinux-common-git", "arcolinux-config-all-desktops-git",
            "arcolinux-dconf-all-desktops-git", "arcolinux-hblock-git", "arcolinux-wallpapers-git", "arconet-wallpapers", "arconet-xfce",
            "nitrogen", "fastfetch", "dconf-editor", "dex", "dmenu", "expac", "gksu", "gvfs", "kitty", "kitty-terminfo", "less", "numlockx",
            "noto-fonts-cjk", "otf-ipafont", "pamixer", "picom", "playerctl", "polybar", "reflector", "ripgrep", "rofi","sddm", "sutils-git",
            "sxhkd", "thunar", "xdo", "thunar-archive-plugin", "thunar-volman",  "tumbler", "udiskie", "xdg-desktop-portal", "xdg-desktop-portal-gtk",
            "xdg-user-dirs", "ttf-hack-nerd", "ttf-jetbrains-mono", "ttf-nerd-fonts-symbols", "ttf-roboto-mono", "awesome-terminal-fonts",
            "noto-fonts", "xtitle-git", "yad", "yay-git", "xarchiver", "file-roller", "tar", "p7zip", "unrar", "unzip", "ttf-font-awesome",
            "ttf-hack", "arcolinux-sddm-simplicity-git", "arc-darkest-theme-git", "tokyonight-gtk-theme-git", "arcolinux-bspwm-git", "arcolinux-polybar-git",
        ]
        install_command = "pacman -S --noconfirm --needed " + " ".join(packages)
        logging.info("Installing BSPWM + extras...")
        self.run_in_chroot(install_command)
        logging.info("Desktop environment installed.")

    def install_bluetooth(self):
        logging.info("Installing and enabling Bluetooth...")
        self.run_in_chroot("pacman -S --noconfirm --needed pulseaudio-bluetooth bluez bluez-libs bluez-utils blueberry")
        self.run_in_chroot("sed -i 's/#AutoEnable=false/AutoEnable=true/g' /etc/bluetooth/main.conf")
        self.run_in_chroot("systemctl enable bluetooth.service")

    def install_printer(self):
        logging.info("Installing printer support (cups, drivers, etc.)...")
        self.run_in_chroot(
            "pacman -S --noconfirm --needed cups cups-pdf ghostscript gsfonts gutenprint "
            "gtk3-print-backends libcups system-config-printer"
        )
        self.run_in_chroot("systemctl enable cups.service")

    def install_pulseaudio(self):
        logging.info("Installing pulseaudio and related packages...")
        self.run_in_chroot(
            "pacman -S --noconfirm --needed pulseaudio pulseaudio-alsa pavucontrol alsa-firmware alsa-lib alsa-plugins alsa-utils "
            "gstreamer gst-plugins-good gst-plugins-bad gst-plugins-base gst-plugins-ugly playerctl volumeicon"
        )

    def install_user_packages(self):
        logging.info("Installing user-level packages...")
        self.run_in_chroot(
            "pacman -S --noconfirm --needed imagewriter insync discord kitty kitty-terminfo meld micro dropbox firefox flameshot "
            "wps-office wps-office-mime yazi libreoffice-fresh bitwarden spotify tree fwupd hunspell-en_us starship sublime-text-4"
        )

    def install_aur_packages(self):
        logging.info("Installing packages from the AUR...")
        aur_packages = [
            "kora-icon-theme",
            "anki",
            "git-credential-manager-bin",
            "clock-rs-git",
            "tokyonight_night"
        ]
        install_command = (
            f'sudo -u {self.username} bash -c "yay -S --noconfirm --needed '
            + " ".join(aur_packages)
            + '"'
        )
        self.run_in_chroot(install_command)

    def set_environment_vars(self):
        logging.info("Setting system-wide environment variables...")
        self.run_in_chroot("echo 'GTK_THEME=Arc-Darkest' >> /etc/environment")
        self.run_in_chroot("echo 'GTK_ICON_THEME_NAME=Kora' >> /etc/environment")
        self.run_in_chroot("echo 'EDITOR=micro' >> /etc/environment")
        self.run_in_chroot("echo 'BROWSER=firefox' >> /etc/environment")
        self.run_in_chroot("echo 'GTK_FONT_NAME=Noto Sans 11' >> /etc/environment")
        self.run_in_chroot("echo 'GTK_CURSOR_THEME_NAME=Bibata-Modern-Ice' >> /etc/environment")

    def config_home(self):
        logging.info("Copying /etc/skel to user's home directory and adjusting ownership...")
        self.run_in_chroot(f"cp -r /etc/skel/. /home/{self.username}/")
        self.run_in_chroot(f"chown -R {self.username}:{self.username} /home/{self.username}")

    def natto_personal_repos(self):
        logging.info("Cloning natto personal repos in chroot...")
        self.run_in_chroot(
            f"git clone --depth 1 'https://gitlab.com/samurailostinjapan/natto-dots.git' "
            f"'/home/{self.username}/natto-dots'"
        )
        self.run_in_chroot(
            f"git clone --depth 1 'https://gitlab.com/samurailostinjapan/natto-japanese.git' "
            f"'/home/{self.username}/natto-japanese'"
        )
        self.run_in_chroot(
            f"git clone --depth 1 'https://gitlab.com/samurailostinjapan/natto-wallpaper.git' "
            f"'/home/{self.username}/natto-wallpaper'"
        )
        self.run_in_chroot(
            f"git clone --depth 1 'https://gitlab.com/samurailostinjapan/natto-repo.git' "
            f"'/home/{self.username}/natto-repo'"
        )
        self.run_in_chroot(
            f"git clone --depth 1 'https://gitlab.com/samurailostinjapan/natto-nemesis.git' "
            f"'/home/{self.username}/natto-nemesis'"
        )
        self.run_in_chroot(f"chown -R {self.username}:{self.username} /home/{self.username}")

    # ------------------ Minimal Only ----------------------
    def run_minimal_install(self):
        logging.info("===== Running MINIMAL install workflow =====")
        self.detect_disk()
        self.partition_disk()
        self.define_partitions()
        self.format_partitions()
        self.mount_partitions()
        self.install_base_system()
        self.generate_fstab()
        self.generate_initramfs()
        self.configure_system()
        self.configure_network()
        self.install_systemd_boot()
        self.set_root_password()
        self.unmount_and_finish()

    # ------------------ Finish ---------------------------

    def unmount_and_finish(self):
        logging.info("Unmounting partitions...")
        subprocess.run(["umount", "-R", "/mnt"], check=True)
        logging.info("Installation complete. Reboot whenever ready.")

# -------------------- MAIN -----------------------------

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="Arch Linux Installer (UEFI-only).")
    parser.add_argument("--efi_size", default="1GiB", help="Size of the EFI partition")
    parser.add_argument("--hostname", default="arch", help="Hostname for the system")
    parser.add_argument("--locale",   default="en_US.UTF-8", help="Locale for the system")
    parser.add_argument("--timezone", default="America/New_York", help="Timezone for the system")
    parser.add_argument("--disk",     default="", help="Disk to install to. If empty, tries /dev/vda, /dev/nvme0n1, /dev/sda.")
    parser.add_argument("--username", default="", help="Username for the new user")
    parser.add_argument("--userpassword", default="", help="Password for the new user")
    parser.add_argument("--rootpassword", default="", help="Password for root")
    parser.add_argument("--kernel",   default="linux", choices=["linux", "linux-lts"], help="Which kernel to install")
    parser.add_argument("--minimal",  action="store_true", help="Perform a minimal installation (skip DE, user creation, etc.)")
    parser.add_argument("--rootspec", default="", help="Fallback root= spec for the bootloader")

    args = parser.parse_args()
    logging.basicConfig(level=logging.INFO)

    # Preliminary checks outside the installer class
    check_root()
    check_uefi()
    ensure_required_packages()
    modify_pacman_conf()

    # Create the ArchInstaller object
    installer = ArchInstaller(args)

    if args.minimal:
        # Minimal path
        installer.prompt_root_password_minimal()
        installer.run_minimal_install()
    else:
        # Full installation
        installer.prompt_credentials()
        installer.detect_disk()
        installer.partition_disk()
        installer.define_partitions()
        installer.format_partitions()
        installer.mount_partitions()
        installer.install_base_system()
        installer.generate_fstab()
        installer.generate_initramfs()
        installer.configure_system()
        installer.configure_network()
        installer.install_systemd_boot()
        installer.set_root_password()
        installer.create_user()
        installer.configure_sudo()
        installer.modify_pacman_conf_in_target()
        installer.add_additional_repos()
        installer.install_display_manager()
        installer.install_bspwm()
        installer.install_bluetooth()
        installer.install_printer()
        installer.install_pulseaudio()
        installer.install_user_packages()
        installer.install_aur_packages()
        installer.set_environment_vars()
        installer.config_home()
        installer.natto_personal_repos()
        installer.unmount_and_finish()
