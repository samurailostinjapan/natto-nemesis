#!/usr/bin/env python3
import os
import configparser
import dbus
import dbus.mainloop.glib
import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

# Paths to your fcitx5 config and profile files
CONFIG_FILE = os.path.expanduser("~/.config/fcitx5/config")
PROFILE_FILE = os.path.expanduser("~/.config/fcitx5/profile")

# Directory that usually has .desktop definitions for input methods
# (Not strictly needed by this generic editor, but included if you want to expand.)
INPUT_METHODS_DIR = "/usr/share/fcitx5/inputmethods"

# DBus info to call ReloadConfig() on fcitx5
FCITX5_DBUS_SERVICE = "org.fcitx.Fcitx5"
FCITX5_DBUS_PATH = "/controller"
FCITX5_DBUS_INTERFACE = "org.fcitx.Fcitx.Controller1"
DBUS_METHOD_RELOAD_CONFIG = "ReloadConfig"

class INIEditorPage(Gtk.Box):
    """
    A single "page" that displays a table of (section, key, value)
    for a given INI file path. Allows adding, removing, and editing entries.
    """
    def __init__(self, file_path):
        super().__init__(orientation=Gtk.Orientation.VERTICAL, spacing=6, margin=10)
        self.file_path = file_path
        self.config = configparser.ConfigParser()

        # ListStore with columns: section (str), key (str), value (str)
        self.store = Gtk.ListStore(str, str, str)

        # A TreeView to display them
        self.treeview = Gtk.TreeView(model=self.store)
        self.treeview.set_vexpand(True)

        # Create columns
        renderer_section = Gtk.CellRendererText()
        column_section = Gtk.TreeViewColumn("Section", renderer_section, text=0)
        self.treeview.append_column(column_section)

        renderer_key = Gtk.CellRendererText()
        column_key = Gtk.TreeViewColumn("Key", renderer_key, text=1)
        self.treeview.append_column(column_key)

        # The value column is editable
        renderer_value = Gtk.CellRendererText()
        renderer_value.set_property("editable", True)
        renderer_value.connect("edited", self.on_value_edited)
        column_value = Gtk.TreeViewColumn("Value", renderer_value, text=2)
        self.treeview.append_column(column_value)

        scrolled_window = Gtk.ScrolledWindow()
        scrolled_window.add(self.treeview)

        # A bar with Add/Remove buttons
        hbox_buttons = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=6)
        self.button_add = Gtk.Button(label="Add Row")
        self.button_add.connect("clicked", self.on_add_row)
        hbox_buttons.pack_start(self.button_add, False, False, 0)

        self.button_remove = Gtk.Button(label="Remove Row")
        self.button_remove.connect("clicked", self.on_remove_row)
        hbox_buttons.pack_start(self.button_remove, False, False, 0)

        self.pack_start(scrolled_window, True, True, 0)
        self.pack_start(hbox_buttons, False, False, 0)

        self.load_file()

    def load_file(self):
        """
        Load the INI file into self.config, then populate self.store
        with all (section, key, value) entries.
        """
        self.store.clear()
        self.config.clear()
        if os.path.isfile(self.file_path):
            self.config.read(self.file_path)
        # Add everything to the ListStore
        for section in self.config.sections():
            for key in self.config[section]:
                val = self.config[section][key]
                self.store.append([section, key, val])

    def save_file(self):
        """
        Write all rows in self.store back to the file (overwriting existing content).
        """
        new_cfg = configparser.ConfigParser()
        # Build up new_cfg from the store
        for row in self.store:
            section, key, val = row
            if not new_cfg.has_section(section):
                new_cfg.add_section(section)
            new_cfg[section][key] = val

        os.makedirs(os.path.dirname(self.file_path), exist_ok=True)
        with open(self.file_path, "w") as f:
            new_cfg.write(f)

    def on_value_edited(self, renderer, path, new_text):
        """
        When user edits the "Value" cell, update self.store accordingly.
        """
        iter_ = self.store.get_iter(path)
        self.store.set_value(iter_, 2, new_text)

    def on_add_row(self, button):
        """
        Add a new row with default placeholders. The user can edit it afterward.
        """
        self.store.append(["NewSection", "NewKey", "NewValue"])

    def on_remove_row(self, button):
        """
        Remove the selected row from the store.
        """
        selection = self.treeview.get_selection()
        model, treeiter = selection.get_selected()
        if treeiter:
            model.remove(treeiter)

class FcitxAdvancedConfigTool(Gtk.Window):
    """
    Main window that has two tabs:
      1) Config (editing ~/.config/fcitx5/config)
      2) Profile (editing ~/.config/fcitx5/profile)
    Plus Reload / Save buttons that call fcitx5 ReloadConfig via DBus.
    """
    def __init__(self):
        super().__init__(title="Fcitx5 Advanced Config Editor")
        self.set_default_size(700, 500)
        self.connect("destroy", Gtk.main_quit)

        # Initialize DBus
        dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
        self.bus = dbus.SessionBus()
        try:
            self.fcitx_obj = self.bus.get_object(FCITX5_DBUS_SERVICE, FCITX5_DBUS_PATH)
            self.fcitx_iface = dbus.Interface(
                self.fcitx_obj, dbus_interface=FCITX5_DBUS_INTERFACE
            )
        except Exception as e:
            print("Warning: Could not connect to fcitx5 DBus service:", e)
            self.fcitx_iface = None

        vbox_main = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)
        self.add(vbox_main)

        self.notebook = Gtk.Notebook()
        vbox_main.pack_start(self.notebook, True, True, 0)

        # Page for config
        self.page_config = INIEditorPage(CONFIG_FILE)
        self.notebook.append_page(self.page_config, Gtk.Label(label="Config File"))

        # Page for profile
        self.page_profile = INIEditorPage(PROFILE_FILE)
        self.notebook.append_page(self.page_profile, Gtk.Label(label="Profile File"))

        # Bottom bar with Reload and Save
        hbox_buttons = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=6)
        vbox_main.pack_start(hbox_buttons, False, False, 0)

        self.reload_button = Gtk.Button(label="Reload")
        self.reload_button.connect("clicked", self.on_reload)
        hbox_buttons.pack_end(self.reload_button, False, False, 0)

        self.save_button = Gtk.Button(label="Save")
        self.save_button.connect("clicked", self.on_save)
        hbox_buttons.pack_end(self.save_button, False, False, 0)

    def on_reload(self, button):
        # Discard changes and reload from disk
        self.page_config.load_file()
        self.page_profile.load_file()
        # Also call ReloadConfig on DBus if available
        if self.fcitx_iface:
            try:
                getattr(self.fcitx_iface, DBUS_METHOD_RELOAD_CONFIG)()
            except Exception as e:
                print("Error calling ReloadConfig via DBus:", e)

    def on_save(self, button):
        # Save both files from the store
        self.page_config.save_file()
        self.page_profile.save_file()
        # Then call ReloadConfig
        if self.fcitx_iface:
            try:
                getattr(self.fcitx_iface, DBUS_METHOD_RELOAD_CONFIG)()
            except Exception as e:
                print("Error calling ReloadConfig via DBus:", e)

if __name__ == "__main__":
    win = FcitxAdvancedConfigTool()
    win.show_all()
    Gtk.main()
