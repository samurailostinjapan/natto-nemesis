#!/bin/bash
#set -e

# Define the desired output and resolution
OUTPUT="Virtual-1"
RESOLUTION="1920x1080"

# Check for Wayland environment and set resolution with wlr-randr
if command -v wlr-randr &> /dev/null; then
    wlr-randr --output "$OUTPUT" --mode "$RESOLUTION"
    echo "Resolution set for Wayland using wlr-randr."
    exit 0
fi

# Check for X11 environment and set resolution with xrandr
if command -v xrandr &> /dev/null; then
    xrandr --output "$OUTPUT" --mode "$RESOLUTION"
    echo "Resolution set for X11 using xrandr."
    exit 0
fi

echo "Neither wlr-randr nor xrandr is available on this system."
