#!/usr/bin/env bash

# Get the list of orphan packages (quietly, just the package names)
orphans=$(pacman -Qdtq)

if [ -z "$orphans" ]; then
    echo "No orphan packages found."
    exit 0
fi

echo "Found the following orphan packages:"
echo "$orphans"
echo

read -p "Do you want to remove these packages? [y/N] " answer

if [[ $answer =~ ^[Yy]$ ]]; then
    sudo pacman -Rns $orphans
else
    echo "No packages were removed."
fi
