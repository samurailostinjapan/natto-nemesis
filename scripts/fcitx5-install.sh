#!/bin/bash
set -euo pipefail

# Installation of FCITX5 software packages
sudo pacman -S --noconfirm --needed \
    fcitx5 \
    fcitx5-qt \
    fcitx5-mozc \
    fcitx5-gtk \
    fcitx5-configtool

# Define configuration file locations
XPROFILE_FILE="$HOME/.xprofile"
POLYBAR_CONFIG_FILE="$HOME/.config/polybar/config.ini"
BSPWM_AUTOSTART="$HOME/.config/bspwm/autostart.sh"

# Set fcitx5 environment variables in .xprofile.
# Since the file is empty, it's safe to overwrite it.
echo "Setting fcitx5 environment variables in $XPROFILE_FILE"
cat <<'EOF' > "$XPROFILE_FILE"
GTK_IM_MODULE=fcitx  
QT_IM_MODULE=fcitx 
XMODIFIERS=@im=fcitx
EOF

# Append fcitx5 module configuration to Polybar's config if it's not already present.
if [ -f "$POLYBAR_CONFIG_FILE" ]; then
    if ! grep -q "^\[module/fcitx5\]" "$POLYBAR_CONFIG_FILE"; then
        echo "Appending fcitx5 configuration to $POLYBAR_CONFIG_FILE"
        cat <<'EOF' >> "$POLYBAR_CONFIG_FILE"
[module/fcitx5]
type = custom/script
exec = [[ $(fcitx5-remote) -eq 2 ]] && echo "JP" || echo "EN"
interval = 1
click-right = fcitx5-configtool &
format-foreground = ${colors.foreground}
format-background = ${colors.background}
EOF
    else
        echo "Polybar config already contains fcitx5 module configuration."
    fi
else
    echo "Polybar config file not found at $POLYBAR_CONFIG_FILE. Skipping Polybar configuration."
fi

# Add fcitx5 to autostart for BSPWM if not already present.
if [ -f "$BSPWM_AUTOSTART" ]; then
    if ! grep -q "fcitx5 &" "$BSPWM_AUTOSTART"; then
        echo "Adding fcitx5 to BSPWM autostart ($BSPWM_AUTOSTART)"
        echo "fcitx5 &" >> "$BSPWM_AUTOSTART"
    else
        echo "BSPWM autostart already contains fcitx5 command."
    fi
else
    echo "BSPWM autostart file not found at $BSPWM_AUTOSTART. Skipping BSPWM autostart configuration."
fi

echo "FCITX5 installation and configuration complete."
