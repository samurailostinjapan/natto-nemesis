#!/bin/bash
set -euo pipefail

# Backup the current /etc/pacman.conf with a timestamp
backup="/etc/pacman.conf.backup-$(date +%Y%m%d-%H%M%S)"
echo "Backing up /etc/pacman.conf to ${backup}"
sudo cp /etc/pacman.conf "${backup}"

# Enable parallel downloads, colored output, and verbose package lists
sudo sed -i "s/^#ParallelDownloads.*/ParallelDownloads = 20/" /etc/pacman.conf
sudo sed -i "s/^#Color/Color/" /etc/pacman.conf
sudo sed -i "s/^#VerbosePkgLists/VerbosePkgLists/" /etc/pacman.conf
sudo sed -i "s/^DownloadUser = alpm/#DownloadUser = alpm/" /etc/pacman.conf

# Insert ILoveCandy after the #DisableSandbox line, if it's not already present
if ! grep -q "ILoveCandy" /etc/pacman.conf; then
    sudo sed -i "/^#DisableSandbox/a ILoveCandy" /etc/pacman.conf
fi

echo "pacman.conf updated successfully."
