#!/usr/bin/env bash
set -euo pipefail

usage() {
    cat <<EOF
Usage: $0 [OPTIONS]

A script for Arch Linux that disables screen blanking and DPMS.

Options:
  --reboot     Reboot the system after applying changes.
  -h, --help   Display this help message.
EOF
    exit 1
}

# Default: do not reboot automatically.
REBOOT_SYSTEM=false

# Parse command-line options.
while [[ "$#" -gt 0 ]]; do
    case "$1" in
        --reboot) REBOOT_SYSTEM=true ;;
        -h|--help) usage ;;
        *) echo "Unknown parameter: $1" >&2; usage ;;
    esac
    shift
done

install_xorg_xset() {
    echo "Installing xorg-xset..."
    sudo pacman -S --needed --noconfirm xorg-xset
}

create_config_directory() {
    echo "Ensuring /etc/X11/xorg.conf.d directory exists..."
    sudo mkdir -p /etc/X11/xorg.conf.d
}

backup_existing_config() {
    local config_file="/etc/X11/xorg.conf.d/10-monitor.conf"
    if sudo test -f "$config_file"; then
        local backup_file="${config_file}.bak.$(date +%Y%m%d%H%M%S)"
        echo "Backing up existing $config_file to $backup_file..."
        sudo cp "$config_file" "$backup_file"
    fi
}

write_config_file() {
    echo "Writing DPMS config to /etc/X11/xorg.conf.d/10-monitor.conf..."
    sudo bash -c 'cat > /etc/X11/xorg.conf.d/10-monitor.conf <<EOF
Section "Monitor"
    Identifier "Monitor0"
    Option "DPMS" "false"
EndSection

Section "ServerFlags"
    Option "BlankTime" "0"
    Option "StandbyTime" "0"
    Option "SuspendTime" "0"
    Option "OffTime" "0"
EndSection
EOF'
}

reboot_system() {
    echo "Rebooting the system..."
    sudo systemctl reboot
}

main() {
    install_xorg_xset
    create_config_directory
    backup_existing_config
    write_config_file

    echo "DPMS has been disabled."
    if $REBOOT_SYSTEM; then
        reboot_system
    else
        echo "To apply changes, please reboot your system."
    fi
}

main
