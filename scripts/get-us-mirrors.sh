#!/bin/bash
set -euo pipefail

# Check if reflector is installed
if ! command -v reflector &> /dev/null; then
    echo "Error: reflector is not installed. Please install it with 'sudo pacman -S reflector' and try again."
    exit 1
fi

# Define the mirrorlist file location
MIRRORLIST="/etc/pacman.d/mirrorlist"

# Backup the existing mirrorlist, if it exists
if [ -f "$MIRRORLIST" ]; then
    echo "Backing up existing mirrorlist to ${MIRRORLIST}.bak"
    sudo cp "$MIRRORLIST" "${MIRRORLIST}.bak"
fi

# Optimize the mirrorlist using reflector
echo "Optimizing mirrorlist with reflector..."
if sudo reflector --country US --age 6 --latest 10 --fastest 10 --download-timeout 3 --protocol https --sort rate --save "$MIRRORLIST"; then
    echo "Mirrorlist updated successfully."
else
    echo "Error updating the mirrorlist."
    exit 1
fi
