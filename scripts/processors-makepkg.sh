#!/usr/bin/env bash
set -euo pipefail

# Determine the number of CPU cores
cpu_cores=$(grep -c ^processor /proc/cpuinfo)
echo "Detected $cpu_cores CPU cores. Configuring settings accordingly..."

# Define file paths
MAKEPKG_CONF="/etc/makepkg.conf"
SUDOERS="/etc/sudoers"

# Function to back up a file (if it exists)
backup_file() {
    local file="$1"
    if [ -f "$file" ]; then
        local timestamp
        timestamp=$(date +%Y%m%d%H%M%S)
        local backup="${file}.bak.${timestamp}"
        echo "Backing up $file to $backup"
        cp "$file" "$backup"
    else
        echo "File $file not found; skipping backup."
    fi
}

# Backup the configuration files
backup_file "$MAKEPKG_CONF"
backup_file "$SUDOERS"

# Update MAKEFLAGS in makepkg.conf
echo "Updating MAKEFLAGS in $MAKEPKG_CONF..."
sed -i -E "s|^#?MAKEFLAGS=.*|MAKEFLAGS=\"-j${cpu_cores}\"|" "$MAKEPKG_CONF"

# Update COMPRESSXZ in makepkg.conf
echo "Updating COMPRESSXZ in $MAKEPKG_CONF..."
sed -i -E "s|^#?COMPRESSXZ=.*|COMPRESSXZ=(xz -c -T ${cpu_cores} -z -)|" "$MAKEPKG_CONF"

# Update /etc/sudoers to uncomment the line for keeping HOME in the environment
echo "Updating $SUDOERS to keep HOME in the environment..."
sed -i '/^# Defaults env_keep += "HOME"/s/^# //' "$SUDOERS"

echo "Configuration updated successfully."
