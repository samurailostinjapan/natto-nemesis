#!/bin/bash
set -euo pipefail

# Ensure wget is installed
sudo pacman -S wget --noconfirm --needed

# Function to download and install a package via pacman
download_and_install() {
    local url="$1"
    local tmp_file="$2"
    local pkg_name="$3"
    
    echo "Downloading $pkg_name..."
    if sudo wget -q "$url" -O "$tmp_file"; then
        echo "Installing $pkg_name..."
        sudo pacman -U --noconfirm --needed "$tmp_file"
        # Remove the temporary file after successful installation.
        rm -f "$tmp_file"
    else
        echo "Failed to download $pkg_name from $url"
        exit 1
    fi
}

# Download and install ArcoLinux keyring
download_and_install "https://github.com/arcolinux/arcolinux_repo/raw/main/x86_64/arcolinux-keyring-20251209-3-any.pkg.tar.zst" \
                     "/tmp/arcolinux-keyring.pkg.tar.zst" \
                     "ArcoLinux keyring"

# Download and install ArcoLinux mirrorlist
download_and_install "https://github.com/arcolinux/arcolinux_repo/raw/main/x86_64/arcolinux-mirrorlist-git-24.03-12-any.pkg.tar.zst" \
                     "/tmp/arcolinux-mirrorlist.pkg.tar.zst" \
                     "ArcoLinux mirrorlist"

# Download and install Chaotic-AUR keyring
download_and_install "https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-keyring.pkg.tar.zst" \
                     "/tmp/chaotic-keyring.pkg.tar.zst" \
                     "Chaotic-AUR keyring"

# Download and install Chaotic-AUR mirrorlist
download_and_install "https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-mirrorlist.pkg.tar.zst" \
                     "/tmp/chaotic-mirrorlist.pkg.tar.zst" \
                     "Chaotic-AUR mirrorlist"

######################################################################################################################
# Function to add a repository block to pacman.conf if it's not already present
add_repo_if_missing() {
    local repo_block="$1"
    local repo_name="$2"

    if ! grep -q "^\[$repo_name\]" /etc/pacman.conf; then
        echo "Adding $repo_name repository to pacman.conf"
        echo "$repo_block" | sudo tee -a /etc/pacman.conf > /dev/null
    else
        echo "$repo_name repository is already present in pacman.conf"
    fi
}

# Define repository blocks (as literal strings so that variables like $repo and $arch remain unexpanded)
arcolinux_repo_block='
[arcolinux_repo]
SigLevel = PackageRequired DatabaseNever
Include = /etc/pacman.d/arcolinux-mirrorlist

[arcolinux_repo_3party]
SigLevel = PackageRequired DatabaseNever
Include = /etc/pacman.d/arcolinux-mirrorlist

[arcolinux_repo_xlarge]
SigLevel = PackageRequired DatabaseNever
Include = /etc/pacman.d/arcolinux-mirrorlist
'

chaotic_aur_block='
[chaotic-aur]
SigLevel = Required DatabaseOptional
Include = /etc/pacman.d/chaotic-mirrorlist
'

natto_repo_block='
[natto-repo]
SigLevel = Optional TrustAll
Server = https://gitlab.com/samurailostinjapan/$repo/-/raw/main/$arch
'

# Add repositories if they are missing
add_repo_if_missing "$arcolinux_repo_block" "arcolinux_repo"
add_repo_if_missing "$chaotic_aur_block" "chaotic-aur"
add_repo_if_missing "$natto_repo_block" "natto-repo"

echo "DONE - Please run 'sudo pacman -Syu' to update the system."
