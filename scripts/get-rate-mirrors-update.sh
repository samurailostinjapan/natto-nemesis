#!/usr/bin/env bash
set -euo pipefail

# Force sudo to prompt for password
sudo -k
sudo true

# Check if rate-mirrors is installed
if ! command -v rate-mirrors &> /dev/null; then
    echo "Error: 'rate-mirrors' is not installed on this system."
    echo "Please install it (e.g. 'sudo pacman -S rate-mirrors') and rerun this script."
    exit 1
fi

echo "Selecting the fastest up-to-date Arch Linux mirrors with rate-mirrors..."

# Run rate-mirrors as per the GitHub documentation:
sudo rate-mirrors --protocol https --allow-root --disable-comments-in-file --save /etc/pacman.d/mirrorlist arch

echo "Mirror list updated successfully!"
