#!/bin/bash
# This script generates an SSH key pair (using Ed25519) and then creates a compressed backup of the key files.
# Make sure to run this script in an environment where ssh-keygen is available.
# Usage: ./generate_and_backup_ssh.sh

# === CONFIGURATION ===
# Set the default email (used as a comment in the SSH key). Change this to your email address.
KEY_COMMENT="your_email@example.com"

# Set the default key type and file name.
KEY_TYPE="ed25519"
DEFAULT_KEY_PATH="$HOME/.ssh/id_${KEY_TYPE}"

# Set the backup directory (it will be created if it does not exist)
BACKUP_DIR="$HOME/ssh_key_backup"

# Generate a timestamp for the backup file
TIMESTAMP=$(date +%Y%m%d%H%M%S)

# === END CONFIGURATION ===

# Ensure the ~/.ssh directory exists
if [ ! -d "$HOME/.ssh" ]; then
    echo "Creating ~/.ssh directory..."
    mkdir -p "$HOME/.ssh"
    chmod 700 "$HOME/.ssh"
fi

# Check if a key already exists and ask if you want to overwrite it.
if [ -f "$DEFAULT_KEY_PATH" ] || [ -f "${DEFAULT_KEY_PATH}.pub" ]; then
    echo "An SSH key already exists at ${DEFAULT_KEY_PATH}."
    read -p "Do you want to overwrite it? (y/n): " overwrite
    if [ "$overwrite" != "y" ]; then
        echo "Exiting without generating a new key."
        exit 0
    fi
fi

# Generate the SSH key.
echo "Generating a new $KEY_TYPE SSH key..."
ssh-keygen -t "$KEY_TYPE" -C "$KEY_COMMENT" -f "$DEFAULT_KEY_PATH"
if [ $? -ne 0 ]; then
    echo "Error: ssh-keygen failed."
    exit 1
fi

# Create the backup directory if it doesn't exist.
mkdir -p "$BACKUP_DIR"

# Create a compressed backup of the key files.
BACKUP_FILE="$BACKUP_DIR/ssh_keys_backup_${TIMESTAMP}.tar.gz"
echo "Backing up your SSH keys to $BACKUP_FILE..."

# Pack both the private and public key files.
tar czvf "$BACKUP_FILE" -C "$HOME/.ssh" "$(basename $DEFAULT_KEY_PATH)" "$(basename $DEFAULT_KEY_PATH).pub"
if [ $? -eq 0 ]; then
    echo "Backup successful!"
else
    echo "Error: Failed to create backup."
    exit 1
fi

echo "SSH key generation and backup complete."
