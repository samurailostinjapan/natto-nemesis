#!/usr/bin/env bash
set -euo pipefail

# -----------------------------------------------------------------------------
# Custom UEFI-only iPXE ISO Builder for ArcoLinux
#
# This script performs the following steps:
# 1. Installs required packages using pacman.
# 2. Removes any existing ipxe directory and clones a fresh copy.
# 3. Creates a custom embedded iPXE script for UEFI environments.
# 4. Builds a UEFI-only iPXE ISO with HTTPS support.
# 5. Renames the built ISO and displays its path.
# -----------------------------------------------------------------------------

install_prerequisites() {
    echo "Installing prerequisites..."
    sudo pacman -Syu --noconfirm base-devel git syslinux nasm edk2-ovmf gnu-efi
}

clone_ipxe_repo() {
    local repo_url="https://github.com/ipxe/ipxe.git"
    if [ -d "ipxe" ]; then
        echo "Removing existing 'ipxe' directory..."
        rm -rf ipxe
    fi
    echo "Cloning ipxe repository from ${repo_url}..."
    git clone "$repo_url"
}

create_embedded_ipxe_script() {
    echo "Creating custom embedded iPXE script..."
    local script_path="ipxe/src/arcolinux.ipxe"
    cat <<'EOF' > "$script_path"
#!ipxe

echo "Welcome to my custom (Arco) Arch iPXE environment!"
echo "DHCP on net0..."
ifopen net0 || goto net_fail
dhcp net0 || goto net_fail

echo "Booting ArcoLinux from a public mirror (2025.01.01)..."

# Just in case iPXE didn’t free memory from prior attempts
imgfree

set mirror mirror.rackspace.com
set date 2025.01.01

kernel http://${mirror}/archlinux/iso/${date}/arch/boot/x86_64/vmlinuz-linux \
       ip=dhcp \
       archiso_http_srv=http://${mirror}/archlinux/iso/${date}/ \
       archisobasedir=arch \
       net.ifnames=0 \
       quiet

initrd http://${mirror}/archlinux/iso/${date}/arch/boot/x86_64/initramfs-linux.img

boot

:net_fail
echo "Network config failed. Press any key to exit..."
prompt
exit
EOF
    echo "Embedded iPXE script created at: ${script_path}"
}

build_uefi_ipxe_iso() {
    echo "Building UEFI-only iPXE ISO with HTTPS support..."
    cd ipxe/src
    make bin-x86_64-efi/ipxe.iso EMBED=arcolinux.ipxe DOWNLOAD_PROTO_HTTPS=1

    echo "Renaming the generated ISO..."
    mv bin-x86_64-efi/ipxe.iso bin-x86_64-efi/arcolinux-ipxe.iso

    local iso_path
    iso_path="$(pwd)/bin-x86_64-efi/arcolinux-ipxe.iso"
    echo "UEFI-only iPXE ISO is available at: ${iso_path}"
}

main() {
    install_prerequisites
    clone_ipxe_repo
    create_embedded_ipxe_script
    build_uefi_ipxe_iso
    echo "Done!"
}

main
