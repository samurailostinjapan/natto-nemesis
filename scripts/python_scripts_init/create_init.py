#!/usr/bin/env python3
import os

def create_init_files(base_dir):
    """Recursively creates __init__.py files in all subdirectories."""
    for root, dirs, files in os.walk(base_dir):
        init_path = os.path.join(root, "__init__.py")
        if not os.path.exists(init_path):
            with open(init_path, "w") as f:
                pass  # Create an empty file
            print(f"Created: {init_path}")

# Run in your project directory
create_init_files("/home/natto/arcoinstall/archinstall/lib/")
