#!/usr/bin/env python3

import os
import ast
import sys

def extract_public_names(file_path):
    """
    Return a list of 'public' names to import from file_path:
      1. If __all__ is defined, use that.
      2. Otherwise, gather top-level classes, functions, and variables not starting with '_'.
    """
    with open(file_path, 'r', encoding='utf-8') as f:
        source = f.read()

    # Attempt to parse the Python source. If it fails, return an empty list.
    try:
        tree = ast.parse(source, filename=file_path)
    except Exception as e:
        print(f"[DEBUG] AST parse failed for {file_path}: {e}", file=sys.stderr)
        return []

    # 1. Check if there's a __all__ assignment
    for node in tree.body:
        if isinstance(node, ast.Assign):
            for target in node.targets:
                if isinstance(target, ast.Name) and target.id == '__all__':
                    try:
                        # Evaluate the __all__ value
                        names = ast.literal_eval(node.value)
                        if isinstance(names, (list, tuple)):
                            # Use exactly what's in __all__
                            public_names = [str(n) for n in names if isinstance(n, str)]
                            print(f"[DEBUG] {file_path} defines __all__ = {public_names}")
                            return public_names
                    except Exception as e:
                        print(f"[DEBUG] Failed to evaluate __all__ in {file_path}: {e}", file=sys.stderr)
                        pass

    # 2. No __all__: gather top-level classes, functions, variables not starting with '_'
    public_names = []
    for node in tree.body:
        if isinstance(node, (ast.FunctionDef, ast.AsyncFunctionDef, ast.ClassDef)):
            if not node.name.startswith('_'):
                public_names.append(node.name)
        elif isinstance(node, ast.Assign):
            for target in node.targets:
                if isinstance(target, ast.Name) and not target.id.startswith('_'):
                    public_names.append(target.id)

    if public_names:
        print(f"[DEBUG] {file_path} top-level public names: {public_names}")
    else:
        print(f"[DEBUG] {file_path} has NO public names (using fallback).")
    # Return them in order, but uniquely
    seen = set()
    unique = []
    for name in public_names:
        if name not in seen:
            seen.add(name)
            unique.append(name)
    return unique


def format_import(module_name, names):
    """
    Return a string import statement:
      - If 'names' is empty, produce:   from . import module_name
      - If single or short, put on one line:    from .module_name import foo, bar
      - If long, split over multiple lines.
    """
    if not names:
        # Fallback: no discovered public names, just import the module
        return f"from . import {module_name}"

    joined = ", ".join(names)
    # If it all fits on one short line, keep it that way
    if len(joined) <= 60:
        return f"from .{module_name} import {joined}"

    # Otherwise break it across lines
    lines = [f"from .{module_name} import ("]
    for name in names:
        lines.append(f"    {name},")
    lines.append(")")
    return "\n".join(lines)


def populate_init_files(base_dir):
    """
    Walk 'base_dir', find .py files in each directory, and generate __init__.py
    with explicit imports. No 'import *' is ever used here.
    """
    print(f"[DEBUG] Scanning base directory: {base_dir}")

    for root, dirs, files in os.walk(base_dir):
        # Filter only .py files, excluding __init__.py
        py_files = [f for f in files if f.endswith(".py") and f != "__init__.py"]
        if not py_files:
            continue

        import_lines = []
        # Sort so output is consistent
        for py_file in sorted(py_files):
            module_name = py_file[:-3]
            file_path = os.path.join(root, py_file)

            # Extract top-level names
            public_names = extract_public_names(file_path)

            # Format the import line
            line = format_import(module_name, public_names)
            import_lines.append(line)

        # If there's something to import, write (or overwrite) __init__.py
        if import_lines:
            init_path = os.path.join(root, "__init__.py")
            print(f"[DEBUG] Writing to {init_path} with {len(import_lines)} import lines")
            with open(init_path, "w", encoding="utf-8") as f:
                f.write("# Auto-generated __init__.py\n")
                for import_line in import_lines:
                    f.write(import_line + "\n")


if __name__ == "__main__":
    base_dir = "/home/natto/arcoinstall/archinstall/lib/"
    if not os.path.isdir(base_dir):
        print(f"[ERROR] Base directory does not exist: {base_dir}", file=sys.stderr)
        sys.exit(1)

    populate_init_files(base_dir)
