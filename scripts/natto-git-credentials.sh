#!/bin/bash
set -euo pipefail

# Set global git config for username and email
git config --global user.name "samurailostinjapan"
git config --global user.email "samurailostinjapan@gmail.com"

# Configure Git to store credentials in plain text (in ~/.git-credentials)
git config --global credential.helper store

# Prompt for GitHub Personal Access Token (input hidden)
echo -n "Enter your GitHub Personal Access Token (input hidden): "
read -rs github_token
echo

# Prompt for GitLab Personal Access Token (input hidden)
echo -n "Enter your GitLab Personal Access Token (input hidden): "
read -rs gitlab_token
echo

# Define the credentials file location
credentials_file="$HOME/.git-credentials"

# Function to add a credential entry if it doesn't already exist
add_credential() {
    local domain="$1"
    local username="$2"
    local token="$3"
    if [ -f "$credentials_file" ]; then
        if grep -q "$domain" "$credentials_file"; then
            echo "Credentials for $domain already exist in $credentials_file."
            return
        fi
    fi
    echo "https://${username}:${token}@${domain}" >> "$credentials_file"
    echo "Added credentials for ${domain}."
}

# Add credentials for GitHub and GitLab
add_credential "github.com" "samurailostinjapan" "$github_token"
add_credential "gitlab.com" "samurailostinjapan" "$gitlab_token"

echo "Git configuration updated with the provided tokens."
