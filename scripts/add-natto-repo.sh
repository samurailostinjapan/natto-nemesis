#!/bin/bash
set -euo pipefail

# Check if [natto-repo] is already present in /etc/pacman.conf
if ! grep -q "^\[natto-repo\]" /etc/pacman.conf; then
    echo "Adding natto-repo to pacman.conf"

    # Backup pacman.conf before modification (optional)
    sudo cp /etc/pacman.conf /etc/pacman.conf.bak && echo "Backup created at /etc/pacman.conf.bak"

    # Append the repository block using a here-doc to avoid inline variable expansion
    sudo tee -a /etc/pacman.conf <<'EOF'
[natto-repo]
SigLevel = Optional TrustAll
Server = https://gitlab.com/samurailostinjapan/$repo/-/raw/main/$arch
EOF

    echo "Repository added successfully."
else
    echo "natto-repo is already present in pacman.conf"
fi

echo "DONE - UPDATE NOW"
