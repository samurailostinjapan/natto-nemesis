#!/usr/bin/env python3
import sys
import os
import gnupg
import subprocess

def backup_keys(gpg):
    public_keys = gpg.list_keys()
    if not public_keys:
        print("No public keys found to backup.")
    else:
        fingerprints = [key['fingerprint'] for key in public_keys]
        public_backup = gpg.export_keys(fingerprints)
        with open("gpg_public_backup.asc", "w") as f:
            f.write(public_backup)
        print("Public keys backed up to gpg_public_backup.asc.")
    
    secret_keys = gpg.list_keys(secret=True)
    if not secret_keys:
        print("No secret keys found to backup.")
    else:
        fingerprints_secret = [key['fingerprint'] for key in secret_keys]
        secret_backup = gpg.export_keys(fingerprints_secret, secret=True)
        with open("gpg_private_backup.asc", "w") as f:
            f.write(secret_backup)
        print("Secret keys backed up to gpg_private_backup.asc.")
    
    try:
        result = subprocess.run(["gpg", "--export-ownertrust"], check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
        with open("ownertrust.txt", "w") as f:
            f.write(result.stdout)
        print("Ownertrust backed up to ownertrust.txt.")
    except subprocess.CalledProcessError as e:
        print("Failed to backup ownertrust:", e)

def restore_keys(gpg):
    if os.path.exists("gpg_public_backup.asc"):
        with open("gpg_public_backup.asc", "r") as f:
            public_backup = f.read()
        import_result = gpg.import_keys(public_backup)
        print("Imported public keys:", import_result.results)
    else:
        print("gpg_public_backup.asc not found. Skipping public key import.")
    
    if os.path.exists("gpg_private_backup.asc"):
        with open("gpg_private_backup.asc", "r") as f:
            secret_backup = f.read()
        import_result_secret = gpg.import_keys(secret_backup)
        print("Imported secret keys:", import_result_secret.results)
    else:
        print("gpg_private_backup.asc not found. Skipping secret key import.")
    
    if os.path.exists("ownertrust.txt"):
        try:
            subprocess.run(["gpg", "--import-ownertrust", "ownertrust.txt"], check=True)
            print("Ownertrust imported from ownertrust.txt.")
        except subprocess.CalledProcessError as e:
            print("Failed to import ownertrust:", e)
    else:
        print("ownertrust.txt not found. Skipping ownertrust import.")

def main():
    if len(sys.argv) < 2:
        print("Usage: {} [backup|restore]".format(sys.argv[0]))
        sys.exit(1)
    
    action = sys.argv[1].lower()
    gpg = gnupg.GPG()
    
    if action == "backup":
        backup_keys(gpg)
    elif action == "restore":
        restore_keys(gpg)
    else:
        print("Invalid action. Use 'backup' or 'restore'.")
        sys.exit(1)

if __name__ == '__main__':
    main()
