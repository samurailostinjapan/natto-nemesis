#!/usr/bin/env bash
set -euo pipefail

# Check that required commands are available
check_prerequisites() {
    local missing=0
    if ! command -v nmcli &>/dev/null; then
        echo "Error: NetworkManager (nmcli) is not installed. Please install it first."
        missing=1
    fi
    if ! command -v wpa_supplicant &>/dev/null; then
        echo "Error: wpa_supplicant is not installed. Please install it first."
        missing=1
    fi
    if [ "$missing" -ne 0 ]; then
        exit 1
    fi
}

# Prompt the user for WiFi credentials
prompt_for_credentials() {
    read -p "Enter SSID: " ssid
    read -sp "Enter Password: " password
    echo
}

# Attempt to connect to the specified WiFi network
connect_wifi() {
    echo "Attempting to connect to WiFi network: $ssid"
    # The following command uses nmcli to connect to the network
    if sudo nmcli dev wifi connect "$ssid" password "$password"; then
        echo "nmcli connection command executed."
    else
        echo "Error: nmcli connection command failed."
        exit 1
    fi
}

# Verify that the connection is active
check_connection() {
    if nmcli con show --active | grep -q "$ssid"; then
        echo "Successfully connected to $ssid."
    else
        echo "Failed to connect to $ssid. Please check the SSID and password."
        exit 1
    fi
}

main() {
    check_prerequisites
    prompt_for_credentials
    connect_wifi
    check_connection
}

main
