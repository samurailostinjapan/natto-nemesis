def disable_dpms(self):
    """
    Disable screen blanking and DPMS using xorg-xset, mirroring your Bash script logic
    but using run_in_chroot and no forced reboot.
    """
    logging.info("Disabling DPMS inside the chroot...")

    # 1. Install xorg-xset
    self.run_in_chroot("pacman -S --needed --noconfirm xorg-xset")

    # 2. Ensure /etc/X11/xorg.conf.d directory
    self.run_in_chroot("mkdir -p /etc/X11/xorg.conf.d")

    # 3. Backup existing 10-monitor.conf, if it exists
    self.run_in_chroot(
        "if [ -f /etc/X11/xorg.conf.d/10-monitor.conf ]; then "
        "cp /etc/X11/xorg.conf.d/10-monitor.conf /etc/X11/xorg.conf.d/10-monitor.conf.bak.$(date +%Y%m%d%H%M%S); "
        "fi"
    )

    # 4. Write the new DPMS config
    dpms_config = """cat <<EOF > /etc/X11/xorg.conf.d/10-monitor.conf
Section "Monitor"
    Identifier "Monitor0"
    Option "DPMS" "false"
EndSection

Section "ServerFlags"
    Option "BlankTime" "0"
    Option "StandbyTime" "0"
    Option "SuspendTime" "0"
    Option "OffTime" "0"
EndSection
EOF
"""
    self.run_in_chroot(dpms_config)

    logging.info("DPMS has been disabled. You may reboot when ready.")
