#!/bin/bash
set -euo pipefail

# Remove legacy iptables to avoid conflicts with iptables-nft
# sudo pacman -Rdd iptables --noconfirm

# Install necessary virtualization packages
sudo pacman -S --noconfirm --needed \
    iptables-nft \
    ebtables \
    qemu-full \
    virt-manager \
    virt-viewer \
    dnsmasq \
    vde2 \
    bridge-utils \
    edk2-ovmf \
    dmidecode

# Enable and start libvirtd service
sudo systemctl enable --now libvirtd.service

# Enable nested virtualization for Intel processors
echo "Enabling nested virtualization for Intel processors..."
echo "options kvm-intel nested=1" | sudo tee -a /etc/modprobe.d/kvm-intel.conf

# Add the current user to the libvirt and kvm groups for VM management privileges
user=$(whoami)
echo "Adding user '${user}' to the libvirt and kvm groups..."
sudo gpasswd -a "$user" libvirt
sudo gpasswd -a "$user" kvm

# Define and enable the default virtual network in libvirt
echo "Setting up the default virtual network..."
sudo virsh net-define /etc/libvirt/qemu/networks/default.xml
sudo virsh net-autostart default

# Restart libvirtd service to apply all changes
sudo systemctl restart libvirtd.service

echo "Reboot your system"