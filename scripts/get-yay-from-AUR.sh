#!/usr/bin/env bash
set -euo pipefail

# This script clones the yay-bin repository from the AUR, builds the package,
# and installs it using pacman.

# Variables
YAY_BIN_DIR="/tmp/yay-bin"
REPO_URL="https://aur.archlinux.org/yay-bin.git"

clone_yay_bin() {
    if [ -d "$YAY_BIN_DIR" ]; then
        echo "Removing existing directory: $YAY_BIN_DIR"
        rm -rf "$YAY_BIN_DIR"
    fi
    echo "Cloning yay-bin repository into $YAY_BIN_DIR..."
    git clone "$REPO_URL" "$YAY_BIN_DIR"
}

build_yay_bin() {
    echo "Navigating to $YAY_BIN_DIR..."
    cd "$YAY_BIN_DIR" || { echo "Failed to navigate to $YAY_BIN_DIR"; exit 1; }
    echo "Building the yay-bin package..."
    makepkg --noconfirm
}

install_yay_bin() {
    echo "Installing the yay-bin package..."
    # Install the package; the package file is expected to match yay-bin*.zst
    sudo pacman -U yay-bin*.zst --noconfirm
}

main() {
    clone_yay_bin
    build_yay_bin
    install_yay_bin
    echo "yay-bin installation complete."
}

main
