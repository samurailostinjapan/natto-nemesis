#!/usr/bin/env python3
import sys
import getpass
import gnupg

def main():
    print("GPG Key Creation Script")
    print("This script will help you create a new GPG key.\n")
    
    name = input("Enter your full name: ").strip()
    email = input("Enter your email address: ").strip()
    comment = input("Enter an optional comment (press enter to skip): ").strip()
    expire_input = input("Enter key expiration (e.g., 1y for one year, 0 for no expiration): ").strip()
    if expire_input == "0":
        expire_date = "0"
    else:
        expire_date = expire_input
        
    passphrase = getpass.getpass("Enter a secure passphrase for your GPG key: ")
    passphrase_confirm = getpass.getpass("Confirm your passphrase: ")
    if passphrase != passphrase_confirm:
        print("Passphrases do not match. Exiting.")
        sys.exit(1)
        
    gpg = gnupg.GPG()
    params = {
        "key_type": "RSA",
        "key_length": 4096,
        "name_real": name,
        "name_email": email,
        "expire_date": expire_date,
        "passphrase": passphrase
    }
    if comment:
        params["name_comment"] = comment
    
    input_data = gpg.gen_key_input(**params)
    
    print("\nGenerating key... Please wait, this may take a while.")
    key = gpg.gen_key(input_data)
    
    if key:
        print("\nGPG key generated successfully.")
        print("Fingerprint:", key.fingerprint)
    else:
        print("\nFailed to generate GPG key.")

if __name__ == '__main__':
    main()
