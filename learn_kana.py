#!/usr/bin/env python3

import json
import os
import random
import tkinter as tk
from tkinter import messagebox

CARDS_FILE = "cards.json"
PROGRESS_FILE = "progress.json"
NUMBER_OF_BOXES = 3  # Adjust as desired (Leitner boxes)

class FlashcardApp:
    def __init__(self, master):
        self.master = master
        self.master.title("Kana Flashcards")

        # Load or init data
        self.cards = self.load_cards()
        self.progress = self.load_progress()

        # Quiz mode: 0 = Kana -> Romaji, 1 = Romaji -> Kana
        self.quiz_mode = 0  # default to Kana -> Romaji

        # UI elements
        self.create_widgets()

        # Start with a new card
        self.current_card = None
        self.load_new_card()

    def create_widgets(self):
        # Mode toggle button
        self.mode_button = tk.Button(self.master, text="Switch to Romaji->Kana",
                                     command=self.toggle_mode)
        self.mode_button.pack(pady=5)

        # Prompt label (kana or romaji)
        self.prompt_label = tk.Label(self.master, text="", font=("Helvetica", 32))
        self.prompt_label.pack(pady=10)

        # Entry for user input
        self.answer_entry = tk.Entry(self.master, font=("Helvetica", 16))
        self.answer_entry.pack(pady=5)
        self.answer_entry.bind("<Return>", lambda e: self.check_answer())

        # Check button
        self.check_button = tk.Button(self.master, text="Check",
                                      command=self.check_answer)
        self.check_button.pack(pady=5)

        # Feedback label
        self.feedback_label = tk.Label(self.master, text="", font=("Helvetica", 14))
        self.feedback_label.pack(pady=5)

        # Next card button
        self.next_button = tk.Button(self.master, text="Next Card",
                                     command=self.load_new_card)
        self.next_button.pack(pady=5)

    def load_cards(self):
        if not os.path.isfile(CARDS_FILE):
            messagebox.showerror("Error", f"Missing {CARDS_FILE}. Please add your kana data.")
            raise SystemExit
        with open(CARDS_FILE, "r", encoding="utf-8") as f:
            return json.load(f)

    def load_progress(self):
        if not os.path.isfile(PROGRESS_FILE):
            # Create empty progress if none
            return {}
        with open(PROGRESS_FILE, "r", encoding="utf-8") as f:
            return json.load(f)

    def save_progress(self):
        with open(PROGRESS_FILE, "w", encoding="utf-8") as f:
            json.dump(self.progress, f, ensure_ascii=False, indent=2)

    def toggle_mode(self):
        self.quiz_mode = 1 - self.quiz_mode
        if self.quiz_mode == 0:
            self.mode_button.config(text="Switch to Romaji->Kana")
        else:
            self.mode_button.config(text="Switch to Kana->Romaji")
        self.load_new_card()

    def pick_card(self):
        """
        Choose a card based on the Leitner box distribution.
        We'll do a simple approach:
        - 60% chance from box 1
        - 30% from box 2
        - 10% from box 3
        Adjust as needed or implement your own weighting.
        """
        # Separate cards by box
        box1 = []
        box2 = []
        box3 = []

        for card in self.cards:
            char = card["character"]
            box = self.progress.get(char, {}).get("box", 1)
            if box == 1:
                box1.append(card)
            elif box == 2:
                box2.append(card)
            else:
                box3.append(card)

        # Weighted choice
        roll = random.random()
        if roll < 0.6 and box1:
            return random.choice(box1)
        elif roll < 0.9 and box2:
            return random.choice(box2)
        else:
            # Either roll >= 0.9 or fallback if others empty
            if box3:
                return random.choice(box3)
            elif box2:
                return random.choice(box2)
            else:
                return random.choice(box1)

    def load_new_card(self):
        self.current_card = self.pick_card()
        self.answer_entry.delete(0, tk.END)
        self.feedback_label.config(text="")

        # Display prompt
        if self.quiz_mode == 0:
            # Show kana, user to input romaji
            self.prompt_label.config(text=self.current_card["character"])
        else:
            # Show romaji, user to input kana
            self.prompt_label.config(text=self.current_card["reading"])

        self.answer_entry.focus()

    def check_answer(self):
        if not self.current_card:
            return

        user_answer = self.answer_entry.get().strip()
        correct_answer = None

        if self.quiz_mode == 0:
            # We showed the kana, correct answer is the reading
            correct_answer = self.current_card["reading"]
        else:
            # We showed the reading, correct answer is the kana
            correct_answer = self.current_card["character"]

        if user_answer == correct_answer:
            self.feedback_label.config(text="Correct!", fg="green")
            self.update_progress(self.current_card["character"], True)
        else:
            # Show the correct answer for clarity
            self.feedback_label.config(text=f"Incorrect! Correct: {correct_answer}", fg="red")
            self.update_progress(self.current_card["character"], False)

    def update_progress(self, char, is_correct):
        card_prog = self.progress.get(char, {"box": 1, "correct": 0, "incorrect": 0})
        if is_correct:
            card_prog["correct"] += 1
            # Move to next box if not at max
            if card_prog["box"] < NUMBER_OF_BOXES:
                card_prog["box"] += 1
        else:
            card_prog["incorrect"] += 1
            # Reset to box 1
            card_prog["box"] = 1

        self.progress[char] = card_prog
        self.save_progress()

def main():
    root = tk.Tk()
    app = FlashcardApp(root)
    root.mainloop()

if __name__ == "__main__":
    main()
