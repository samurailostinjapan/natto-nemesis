#!/usr/bin/env bash
set -euo pipefail

# Adjust these if needed
USERNAME="natto"
DOTS_DIR="/home/$USERNAME/natto-dots"

# Ensure the dotfiles repo actually exists
if [[ ! -d "$DOTS_DIR" ]]; then
    echo "ERROR: $DOTS_DIR does not exist. Make sure you have cloned natto-dots into /home/$USERNAME."
    exit 1
fi

# Array of "destination:source" pairs
# Destination is relative to /home/$USERNAME
# Source is relative to $DOTS_DIR
dotfiles=(
    ".bashrc:.bashrc"
    ".zshrc:.zshrc"
    ".xprofile:.xprofile"
    ".Xresources:.Xresources"
    ".screenrc:.screenrc"
    ".fehbg:.fehbg"
    ".gtkrc-2.0:.gtkrc-2.0"
    ".gitconfig:.gitconfig"
    ".user-dirs.dirs:user-dirs.dirs"
    ".config/alacritty/alacritty.toml:alacritty/alacritty.toml"
    ".config/bspwm/launcher/rofi/colors.rasi:bspwm/launcher/rofi/colors.rasi"
    ".config/bspwm/launcher/rofi/launcher.rasi:bspwm/launcher/rofi/launcher.rasi"
    ".config/bspwm/launcher/launcher.sh:bspwm/launcher/launcher.sh"
    ".config/bspwm/scripts/wofi-keybinds-cheatsheet.sh:bspwm/scripts/wofi-keybinds-cheatsheet.sh"
    ".config/bspwm/scripts/rofi-keybinds-cheatsheet.sh:bspwm/scripts/rofi-keybinds-cheatsheet.sh"
    ".config/bspwm/scripts/pseudo_automatic_mode:bspwm/scripts/pseudo_automatic_mode"
    ".config/bspwm/scripts/picom-toggle.sh:bspwm/scripts/picom-toggle.sh"
    ".config/bspwm/scripts/move-window:bspwm/scripts/move-window"
    ".config/bspwm/scripts/external_rules_command:bspwm/scripts/external_rules_command"
    ".config/bspwm/scripts/adjust-new-window:bspwm/scripts/adjust-new-window"
    ".config/bspwm/sxhkd/sxhkdrc:bspwm/sxhkd/sxhkdrc"
    ".config/bspwm/autostart.sh:bspwm/autostart.sh"
    ".config/bspwm/bspwmrc:bspwm/bspwmrc"
    ".config/bspwm/arco-wallpaper.jpg:bspwm/arco-wallpaper.jpg"
    ".config/bspwm/picom.conf:bspwm/picom.conf"
    ".config/clock-rs/conf.toml:clock-rs/conf.toml"
    ".config/fastfetch/config.jsonc:fastfetch/config.jsonc"
    ".config/fcitx5/config:fcitx5/config"
    ".config/fcitx5/profile:fcitx5/profile"
    ".config/gtk-2.0/gtkfilechooser.ini:gtk-2.0/gtkfilechooser.ini"
    ".config/gtk-2.0/gtkrc:gtk-2.0/gtkrc"
    ".config/gtk-3.0/bookmarks:gtk-3.0/bookmarks"
    ".config/gtk-3.0/settings.ini:gtk-3.0/settings.ini"
    ".config/gtk-4.0/settings.ini:gtk-4.0/settings.ini"
    ".config/kitty/kitty.conf:kitty/kitty.conf"
    ".config/nitrogen/bg-saved.cfg:nitrogen/bg-saved.cfg"
    ".config/nitrogen/nitrogen.cfg:nitrogen/nitrogen.cfg"
    ".config/polybar/config.ini:polybar/config.ini"
    ".config/polybar/launch.sh:polybar/launch.sh"
    ".config/polybar/scripts/get_spotify_status.sh:polybar/scripts/get_spotify_status.sh"
    ".config/polybar/scripts/pavolume.sh:polybar/scripts/pavolume.sh"
    ".config/polybar/scripts/pub-ip.sh:polybar/scripts/pub-ip.sh"
    ".config/polybar/scripts/scroll_spotify_status.sh:polybar/scripts/scroll_spotify_status.sh"
    ".config/polybar/scripts/spotify1.sh:polybar/scripts/spotify1.sh"
    ".config/rofi/config.rasi:rofi/config.rasi"
    ".config/starship/starship.toml:starship/starship.toml"
    ".config/sublime-text/tokyonight_night.tmTheme:sublime-text/tokyonight_night.tmTheme"
    ".config/volumeicon/volumeicon:volumeicon/volumeicon"
    ".config/yazi/tokyonight_night.toml:yazi/tokyonight_night.toml"
)

# Copy each file to the correct location under /home/$USERNAME
for filepair in "${dotfiles[@]}"; do
    dest="${filepair%%:*}"
    src="${filepair##*:}"

    # Make sure the destination folder exists
    mkdir -p "/home/$USERNAME/$(dirname "$dest")"

    # Copy the file
    cp -f "$DOTS_DIR/$src" "/home/$USERNAME/$dest"
done

# Adjust ownership
chown -R "$USERNAME:$USERNAME" "/home/$USERNAME"

echo "Dotfiles copied successfully to /home/$USERNAME."
